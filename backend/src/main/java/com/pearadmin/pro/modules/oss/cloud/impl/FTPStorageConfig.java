package com.pearadmin.pro.modules.oss.cloud.impl;

import cn.hutool.extra.ftp.Ftp;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "upload.ftp")
public class FTPStorageConfig {
    private String host;

    private int port;

    private String user;

    private String password;

    private String rootPath;

    @Bean
    public Ftp ftp() {
        return new Ftp(host, port, user, password);
    }
}
