package com.pearadmin.pro.modules.oss.cloud;

import com.pearadmin.pro.modules.oss.domain.SysOss;
import org.springframework.web.multipart.MultipartFile;

import java.io.OutputStream;

public interface StorageService {

    /**
     * 文件上传
     *
     * @param file 文件
     *
     * @return {@link String} 存储路径
     * */
    String upload(MultipartFile file);

    public void download(SysOss sysOss, OutputStream outputStream);
}
